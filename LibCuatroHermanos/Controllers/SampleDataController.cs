using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibCuatroHermanos.Data;
using LibCuatroHermanos.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LibCuatroHermanos.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private readonly LibraryContext _context;

        public SampleDataController(LibraryContext context)
        {
            _context = context;
        }

        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var categoria = new Categoria()
            {
                Nombre = "Libros"
            };

            using (var context = _context)
            {
                context.Add(categoria);
                context.SaveChanges();
            }

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });

        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
