﻿using AutoMapper;
using LibCuatroHermanos.Domain;
using LibCuatroHermanos.Dtos;

namespace LibCuatroHermanos.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Usuario, UsuarioDto>();
            CreateMap<UsuarioDto, Usuario>();
        }
    }
}