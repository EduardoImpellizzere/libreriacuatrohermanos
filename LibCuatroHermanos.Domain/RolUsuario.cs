﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class RolUsuario
    {
        [Key]
        [Required]
        public int RolUsuarioId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Nombre { get; set; }
        [MaxLength(45)]
        public string Descripcion { get; set; }

        public List<PermisoRolUsuario> Permisos { get; set; }
    }
}