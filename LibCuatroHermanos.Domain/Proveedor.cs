﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Proveedor
    {
        [Required]
        public int ProveedorId { get; set; }
        [Required]
        public int DomicilioId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(45)]
        public string Telefono { get; set; }

        public Domicilio Domicilio { get; set; }
    }
}