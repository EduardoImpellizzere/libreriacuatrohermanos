﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class PermisoRolUsuario
    {
        [Required]
        public int PermisoRolUsuarioId { get; set; }
        [Required]
        public int CaracteristicaId { get; set; }
        [Required]
        public int RolUsuarioId { get; set; }

        public Caracteristica Caracteristica { get; set; }
        public RolUsuario RolUsuario { get; }
    }
}