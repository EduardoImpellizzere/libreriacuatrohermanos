﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Compra
    {
        [Required]
        public int CompraId { get; set; }
        [Required]
        public int ProveedorId { get; set; }
        [Required]
        public DateTime FechaCompra { get; set; }
        [Required]
        public decimal Total { get; set; }

        public Proveedor Proveedor { get; set; }
        public List<LineaCompra> LineasCompra { get; set; }
    }
}