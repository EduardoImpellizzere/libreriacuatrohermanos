To run the solution you should have the following:

    .NET Core SDK - includes the .NET Core runtime and command line tools
        https://www.microsoft.com/net/download/core
        
    Visual Studio 2017 
    OR
    Visual Studio Code - code editor that runs on Windows, Mac and Linux
    
    C# extension for Visual Studio Code - adds support to VS Code for developing .NET Core applications
    
    Local DB: SQL SERVER 2017
