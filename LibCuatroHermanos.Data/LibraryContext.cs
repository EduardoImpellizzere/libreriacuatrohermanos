﻿using LibCuatroHermanos.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace LibCuatroHermanos.Data
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        {
        }

        public static readonly LoggerFactory ConsoleLoggerFactory = new LoggerFactory(new []
        {
            new ConsoleLoggerProvider((category, level) => category == DbLoggerCategory.Name && level == LogLevel.Information, true)
        });

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(ConsoleLoggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity("LibCuatroHermanos.Domain.Venta", b =>
            {
                b.HasOne("LibCuatroHermanos.Domain.Cliente", "Cliente")
                    .WithMany()
                    .HasForeignKey("ClienteId")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne("LibCuatroHermanos.Domain.Usuario", "Usuario")
                    .WithMany()
                    .HasForeignKey("UsuarioId")
                    .OnDelete(DeleteBehavior.Restrict);
            });
        }

        #region DBSets
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Compra> Compras { get; set; }
        public DbSet<Venta> Ventas { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Caracteristica> Caracteristicas { get; set; }
        public DbSet<CargoEmpleado> CargoEmpleados { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<ConceptoSueldo> ConceptoSueldos { get; set; }
        public DbSet<DetalleLiquidacionSueldo> DetalleLiquidacionSueldos { get; set; }
        public DbSet<Domicilio> Domicilios { get; set; }
        public DbSet<LineaCompra> LineaCompras { get; set; }
        public DbSet<LineaVenta> LineaVentas { get; set; }
        public DbSet<LiquidacionSueldo> LiquidacionSueldos { get; set; }
        public DbSet<PermisoRolUsuario> PermisosRolUsuarios { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Proveedor> Proveedores { get; set; }
        public DbSet<RolUsuario> RolUsuarios { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        #endregion
    }
}